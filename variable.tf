variable "ami" {
   type        = string
   description = "Ubuntu AMI ID in N. Virginia Region"
   default     = "ami-080e1f13689e07408"  # Default Ubuntu 20.04 LTS AMI
}

variable "instance_type" {
   type        = string
   description = "Instance type (e.g., t2.micro, t3.medium, etc.)"
   default     = "t2.micro"
}

variable "name_tag" {
   type        = string
   description = "Name tag for the EC2 instance"
   default     = "My EC2 Instance"
}
